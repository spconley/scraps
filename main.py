import os
import time
import json
import requests
from bs4 import BeautifulSoup

class RecipeSource:
	def __init__(self, url, id, source=''):
		self.url = url
		self.id = id
		self.source = source

		if self.source == '':
			self.source = self.get_source()

	def get_source(self):
		if 'allrecipes.com' in self.url:
			return 'allrecipes'
		return 'unknown'

recipeSources = []
recipesJSONs = []



def save_html(html, path):
	if not path[-5:] == '.html':
		path += '.html'

	with open(path, 'wb') as f:
		f.write(html)

def open_html(path):
	if not path[-5:] == '.html':
		path += '.html'

	with open(path, 'rb') as f:
		return f.read()

def to_relative_path(folder, file):
	return os.path.join(os.path.dirname(__file__), folder, file)

def recipe_from_allrecipes(rs):
	# TODO Move code that will become redundant when other sources are added
	folder = 'cache'
	htmlPath = to_relative_path(folder, rs.id + '.html')
	if not os.path.exists(folder):
		os.makedirs(folder)
	if not os.path.exists(htmlPath):
		print(htmlPath, 'does not exist, pulling from', rs.url)
		r = requests.get(rs.url)
		save_html(r.content, htmlPath)

	soup = BeautifulSoup(open_html(htmlPath), 'html.parser')
	html_json = soup.select_one('script[type="application/ld+json"]').string
	pure_json = json.loads(html_json)
	recipe_json = pure_json[1]

	recipe_json['_meta'] = {}
	recipe_json['_meta']['id'] = rs.id

	return recipe_json



with open('sources.json', 'r') as f:
    obj = json.loads(f.read())
    for entry in obj:
    	if not 'source' in entry:
    		entry['source'] = ''
    	rs = RecipeSource(entry['url'], entry['id'], entry['source'])
    	recipeSources.append(rs)

# TODO Add more efficient rate limiting for each source
for rs in recipeSources:
	if (rs.source == 'allrecipes'):
		recipesJSONs.append(recipe_from_allrecipes(rs))
		time.sleep(1)

md_headline = ['====== ', ' ======']
md_bold = '**'
md_code = '\'\''
md_ul = '  *'
md_ol = '  -'

for rj in recipesJSONs:
	outTextArray = []
	folder = 'output'
	outTextPath = to_relative_path(folder, rj['_meta']['id'] + '.md')
	if not os.path.exists(folder):
		os.makedirs(folder)

	name = rj['name']
	author = rj['author'][0]['name']
	description = rj['description']
	url = rj['mainEntityOfPage']
	yieldQuantity = rj['recipeYield']
	ingredients = []
	for ingredient in rj['recipeIngredient']:
		ingredients.append(ingredient.replace(u'\u2009', ' '))
	instructions = []
	for instruction in rj['recipeInstructions']:
		instructionText = instruction['text'].replace('\n', '')
		instructionText.replace(u'\u2009', ' ')
		instructions.append(instructionText)
	nutritions = rj['nutrition']

	outTextArray.append(md_headline[0] + name + md_headline[1])
	outTextArray.append(md_bold + 'Yield:' + md_bold + ' ' + yieldQuantity + '\n')
	outTextArray.append(md_bold + 'Author:' + md_bold + ' ' + author + '\n')
	outTextArray.append(md_bold + 'Description:' + md_bold + ' ' + description + '\n')
	if url:
		outTextArray.append(md_bold + 'URL:' + md_bold + ' ' + url + '\n')

	outTextArray.append(md_bold + 'Ingredients:' + md_bold)
	for ingredient in ingredients:
		outTextArray.append(md_ul + ingredient)

	outTextArray.append(md_bold + 'Instructions:' + md_bold)
	for instruction in instructions:
		outTextArray.append(md_ol + instruction)

	outTextArray.append(md_bold + 'Nutrition:' + md_bold)
	for entry in nutritions:
		if (entry == '@type' or not type(nutritions[entry]) == str):
			continue
		
		prettyEntry = entry
		entryLen = len(prettyEntry)
		i = 0
		while i < entryLen:
			if prettyEntry[i].isupper():
				prettyEntry = prettyEntry[:i] + ' ' + prettyEntry[i:]
				i += 1
				entryLen += 1
			i += 1

		prettyEntry = prettyEntry[0].capitalize() + prettyEntry[1:]

		outTextArray.append('  ' + prettyEntry + ': ' + nutritions[entry])

	with open(outTextPath, 'w') as f:
		for line in outTextArray:
			f.write(line + '\n')